# Illustration Detector
The three stage workflow described [here](README.md) produced a dataset of 3864 illustrations extracted from 3629 images in the [NLS Chapbooks dataset](https://doi.org/10.34812/vb2s-9g58). This dataset can be used to train an object detector for detecting illustrations in early printed books. In this tutorial, we describe the training of a [EfficientDet](https://arxiv.org/abs/1911.09070) object detector model for detecting illustrations. 

## Using the Trained Illustration Detector
```
export MYSTORE=$HOME/nls                 # we have been storing everything here
source $MYSTORE/venv/bin/activate        # see README.md to install all dependencies

# required only if you do not already have clones the nls-chapbooks-illustration repo.
mkdir -p $MYSTORE/illustration-detector
cd $MYSTORE/illustration-detector
git clone --recurse-submodules https://gitlab.com/vgg/nls-chapbooks-illustrations.git
cd $MYSTORE/nls-chapbooks-illustrations/tools
export PYTHONPATH=$MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
reset && python3 detect-illustration.py \
  --model-name=efficientdet-d0 \
  --saved-model-dir=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/saved_model/v1/  \
  --hparams=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/hparams.yaml \
  --input-image=$MYSTORE/nls-chapbooks-illustrations/data/images/test_images/*.jpg \
  --output-image-dir=$MYSTORE/nls-chapbooks-illustrations/data/images/test_images_detection/ \
  --output-json-fn=$MYSTORE/nls-chapbooks-illustrations/data/images/test_images_detection/metadata.json
```
The [tools/detect-illustration.py](tools/detect-illustration.py) script applies the illustration
detector to all the images in the folder pointed by the `--input-image` flag. The detection results
are saved as images in the folder pointed by the `--output-image-dir` flag. The detection metadata
(e.g. bounding box coordinates, detection confidence) to the JSON file pointed by the
`--output-json-fn` flag.

Alternatively, you can also use the `model_inspect.py` that is included in the efficientdet
code repository.

```
cd $MYSTORE/illustration-detector/nls-chapbooks-illustrations/automl/efficientdet/
python model_inspect.py --runmode=saved_model_infer \
  --model_name=efficientdet-d0  \
  --saved_model_dir=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/saved_model/v1/  \
  --input_image=$MYSTORE/nls-chapbooks-illustrations/data/images/test_images/*.jpg \
  --hparams=$MYSTORE/nls-chapbooks-illustrations/data/efficientdet/hparams.yaml \
  --output_image_dir=$MYSTORE/detections/
```


## Illustration Detector Training using Full Chapbooks Illustration Dataset
Here, we assume that you have followed the installation procedure described in README.md

```
export MYSTORE=$HOME/nls                 # we have been storing everything here
source $MYSTORE/venv/bin/activate

## export full illustration from LISA project annotations into COCO format
## Training set image count = 3629 * 2
## Number of images with illustration in training set = 3629
## Number of images without illustration in training set = 3629
mkdir -p $MYSTORE/tfrecord/nls_chapbooks/full-model-training/
cd $MYSTORE/nls-chapbooks-illustrations/tools/
python lisa-to-coco-full-training.py \
  --lisa_project_fn=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step5-manual-verification-image-20000-to-47329.json \
  --coco_fn=$MYSTORE/tfrecord/nls_chapbooks/full-model-training/all.json

## convert COCO format anntoations to tfrecord (for EfficientDet training)
cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
PYTHONPATH=".:$PYTHONPATH" python dataset/create_coco_tfrecord.py --logtostderr \
  --image_dir=$MYSTORE/nls-data-chapbooks/ \
  --object_annotations_file=$MYSTORE/tfrecord/nls_chapbooks/full-model-training/all.json \
  --output_file_prefix=$MYSTORE/tfrecord/nls_chapbooks/full-model-training/train \
  --num_shards=1

  > I0906 03:50:59.530666 139840623251584 create_coco_tfrecord.py:215] Building bounding box index.
  > I0906 03:50:59.533040 139840623251584 create_coco_tfrecord.py:226] 3629 images are missing bboxes.
  > ...
  > ...
  > I0906 03:51:09.905596 139840623251584 create_coco_tfrecord.py:323] On image 7200 of 7258
  > I0906 03:51:10.028641 139840623251584 create_coco_tfrecord.py:334] Finished writing, skipped 1 annotations.

## start training of EfficientDet object detector on chapbook illustrations
## we train efficientdet-d0 model (instead of efficientdet-d2 model) because
## it is requires less GPU resources and trains faster.
mkdir -p $MYSTORE/models/full-model-training/
echo $'num_classes: 1\nmoving_average_decay: 0\nlabel_map: { 1: \'Early Printed Illustration\' }' > $MYSTORE/models/full-model-training/hparams.yaml

cd $MYSTORE/nls-chapbooks-illustrations/automl/efficientdet/
wget https://storage.googleapis.com/cloud-tpu-checkpoints/efficientdet/coco/efficientdet-d0.tar.gz
tar xf efficientdet-d0.tar.gz

python main.py --mode=train \
  --train_file_pattern=$MYSTORE/tfrecord/nls_chapbooks/all/train-*-of-00008.tfrecord \
  --model_name=efficientdet-d0 \
  --model_dir=$MYSTORE/models/full-model-training/ \
  --ckpt=efficientdet-d0 \
  --train_batch_size=8 \
  --num_examples_per_epoch=7258 --num_epochs=10  \
  --hparams=$MYSTORE/models/full-model-training/hparams.yaml \
  --eval_after_train=false --tf_random_seed=9973

## export the model for inference
mkdir -p $MYSTORE/models/full-model-training/saved_model
mkdir -p $MYSTORE/models/full-model-training/saved_model
saved_model/v1/
python model_inspect.py --runmode=saved_model \
  --model_name=efficientdet-d0 \
  --ckpt_path=$MYSTORE/models/full-model-training/ \
  --hparams=$MYSTORE/models/full-model-training/hparams.yaml \
  --saved_model_dir=$MYSTORE/models/full-model-training/saved_model

## apply the trained illustration detector on a new image downloaded from
## the British library
## image source: https://www.bl.uk/britishlibrary/~/media/bl/global/dst%20discovering%20sacred%20texts/collection%20items/tyndales-new-testament-1526-c_188_a_17_f001r.jpg

python model_inspect.py --runmode=saved_model_infer \
  --model_name=efficientdet-d0  \
  --saved_model_dir=$MYSTORE/models/full-model-training/saved_model  \
  --input_image=$MYSTORE/nls-chapbooks-illustrations/data/images/test_images/*.jpg \
  --hparams=$MYSTORE/models/full-model-training/hparams.yaml \
  --output_image_dir=$MYSTORE/detections/
```

The following [image](https://www.bl.uk/collection-items/william-tyndales-new-testament) shows the result of applying illustration detector on a new image (i.e. image not seen during training) <a href="https://www.bl.uk/collection-items/william-tyndales-new-testament">downloaded from the British Library</a>.

<img src="data/images/test_images_detection/BL_tyndales-new-testament-1526-c_188_a_17_f001r.jpg" height="400" title="Illustration detected by the trained illustration detection on a British Library archive image downloaded from https://www.bl.uk/britishlibrary/~/media/bl/global/dst%20discovering%20sacred%20texts/collection%20items/tyndales-new-testament-1526-c_188_a_17_f001r.jpg">



## Cross Validation to Estimate Performance of Illustration Detector
We use the [leave-one-out](https://en.wikipedia.org/wiki/Cross-validation_(statistics)#Leave-p-out_cross-validation) cross-validation technique to estimate the performance of illustration detector trained on the full chapbook illustration dataset.

The NLS Chapbooks dataset contains 47329 images out of which only 3629 images contains one or more illustrations. So, we split the set of 3629 images into 10 subset. One of these subsets are used as test set and the remaining 9 subsets are used for training. This strategy allows us to estimate performance variations over the full illustration dataset. For training, we also add equal number of images that do not contain an illustration. In the test set, we add all the remaining images that do not contain an illustration.

```
export MYSTORE=$HOME/nls                 # we have been storing everything here
source $MYSTORE/venv/bin/activate

##
## Leave-one-out cross validation
##
## Total = 47329 images
## Illustration images = 3629
## Non-illustration images = 43700
##
## Training Set : 3267 illustration images + rand-3267 non-illustration images
## Test Set     : 362 illustration images  + remaining 40433 non-illustration images
export MYSTORE=$HOME/nls
source $MYSTORE/venv/bin/activate
mkdir -p $MYSTORE/k-fold-cross-validation/
cd $MYSTORE/nls-chapbooks-illustrations/tools/
python lisa-to-k-fold-cross-validation-coco-dataset.py \
  --lisa_project_fn=$MYSTORE/nls-chapbooks-illustrations/data/annotations/step5-manual-verification-image-20000-to-47329.json \
  --base_outdir=$MYSTORE/k-fold-cross-validation/ \
  --kfold=10

./run-k-fold-cross-validation.sh

## to visualise training and test performance
tensorboard --bind_all --logdir $MYSTORE/k-fold-cross-validation/
```

The following plot show Average Precision (AP) of 
illustration detector for each of the possible 10 test subsets. The AP is computed at Intersection over Union (IoU) threshold of 0.75.

<img src="data/cross-validation/cross-validation-10fold-AP75.jpg" height="200" title="Average precision (AP) on 10 test subsets of the leave-one-out cross validation experiment. The AP is computed at Intersection over Union (IoU) threshold of 0.75.">

The following plot show Average Recall (AR) of 
illustration detector for each of the possible 10 test subsets. The AR is computed over all the Intersection over Union (IoU) threshold values in the range of 0.5:0.95 and considers a maximum of 100 detections.

<img src="data/cross-validation/cross-validation-10fold-ARmax100.jpg" height="200" title="Average recall (AR) on 10 test subsets of the leave-one-out cross validation experiment. The AR is computed over all the Intersection over Union (IoU) threshold values in the range of 0.5:0.95 and considers a maximum of 100 detections.">


## Contact
Contact [Abhishek Dutta](adutta@robots.ox.ac.uk) for feedback and queries related to this code repository.