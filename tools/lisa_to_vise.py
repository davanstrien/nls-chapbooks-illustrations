# Export LISA project in VISE format
# - export images in "image_src" folder
# - export image regions in "image_regions" folder
# - export regions to "region_metadata" table in metadata_db.sqlite database
#
# Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
# Date: 10 Dec. 2020
#
# Run using: $python3 lisa_project_to_vise.py ...
#
# Updates:
# - ...

import json
from json import encoder
import random
import os
import datetime
import sqlite3
import argparse
from PIL import Image

from xml.dom import minidom

FLOAT_PRECISION = 2

def load_chapbooks_metadata_from_json(chapbook_id_list, chapbooks_metadata, JSON_METADATA_DIR):
  print('Loading metadata from JSON: %s' % (JSON_METADATA_DIR))
  for chapbook_id in chapbook_id_list:
    ## load metadata from extra JSON files provided by NLS
    places_json_fn = os.path.join(JSON_METADATA_DIR, 'places', chapbook_id + '.json')
    people_json_fn = os.path.join(JSON_METADATA_DIR, 'people', chapbook_id + '.json')

    chapbook_metadata = { 'place_printed':'',
                          'place_printed_nation':'',
                          'place_published':'',
                          'place_sold':'',
                          'publisher':'',
                          'printer':'',
                          'bookseller':''
    }
    
    if os.path.exists(places_json_fn):
      f = open(places_json_fn, 'r')
      places_json = json.load(f)
      f.close()
      
      if len(places_json) != 0:
        place_printed_found = False
        place_printed_index = -1;
        for i in range(0, len(places_json)):
          if places_json[i]['placeTypeTableID'] == 22: # i.e. placeType = 'Place printed'
            # "Europe, United Kingdom, Scotland, Aberdeen, Aberdeen (inhabited place)"
            place = places_json[i]['place']
            place_tok = place.split(',')
            n = len(place_tok)
            if n == 5:
              place_printed = place_tok[n-1].split('(')[0].strip()
              chapbook_metadata['place_printed'] = place_printed;
              chapbook_metadata['place_printed_nation']= place_tok[2].strip()
          if places_json[i]['placeTypeTableID'] == 18: # placeType = 'Place published'
            # "Europe, United Kingdom, Scotland, Aberdeen, Aberdeen (inhabited place)"
            place = places_json[i]['place']
            place_tok = place.split(',')
            n = len(place_tok)
            if n == 5:
              place_published = place_tok[n-1].split('(')[0].strip()
              chapbook_metadata['place_published'] = place_published
          # no entries for "Place sold", so ignore

    if os.path.exists(people_json_fn):
      f = open(people_json_fn, 'r')
      people_json = json.load(f)
      f.close()
      if len(people_json) != 0:
        chapbook_metadata['publisher'] = ''
        chapbook_metadata['printer'] = ''
        chapbook_metadata['bookseller'] = ''

        for people_json_i in people_json:
          if people_json_i['whoType'] == 'Publisher':
            chapbook_metadata['publisher'] = people_json_i['who'].strip()
          if people_json_i['whoType'] == 'Printer':
            chapbook_metadata['printer'] = people_json_i['who'].strip()
          if people_json_i['whoType'] == 'Bookseller':
            chapbook_metadata['bookseller'] = people_json_i['who'].strip()

    chapbooks_metadata[chapbook_id] = chapbook_metadata
        
  return chapbooks_metadata

def load_chapbooks_metadata_from_mets(chapbook_id_list,
                                      chapbooks_metadata,
                                      NLS_IMAGE_DIR):
  print('Loading metadata from METS XML: %s' % (NLS_IMAGE_DIR))
  for chapbook_id in chapbook_id_list:
    ## load metadata from METS XML files
    chapbooks_metadata[chapbook_id]['volume'] = ''
    chapbooks_metadata[chapbook_id]['item'] = ''
    chapbooks_metadata[chapbook_id]['imprint'] = ''
    chapbooks_metadata[chapbook_id]['date'] = ''
    
    mets_fn = os.path.join(NLS_IMAGE_DIR, chapbook_id, chapbook_id + '-mets.xml')
    if not os.path.exists(mets_fn):
      print('missing: ' + mets_fn)
      continue
    mets = minidom.parse(mets_fn)
    dmdSec = mets.getElementsByTagName('dmdSec')
    for dmdSec_item in dmdSec:
      mdWrap = dmdSec_item.getElementsByTagName('mdWrap')
      for mdwrap_item in mdWrap:
        if mdwrap_item.attributes['LABEL'].value == 'Holdings metadata':
          # <mods:shelfLocator>L.C.2786.A(1)</mods:shelfLocator>
          shelfLocator = mdwrap_item.getElementsByTagName('mods:shelfLocator')[0].firstChild.nodeValue
          item = shelfLocator.strip()
          volume = ''
          if '(' in shelfLocator and ')' in shelfLocator:
            volume = shelfLocator.split('(')[0]
            chapbooks_metadata[chapbook_id]['volume'] = volume.strip()
            chapbooks_metadata[chapbook_id]['item'] = item.strip()
            
        if mdwrap_item.attributes['LABEL'].value == 'Bibliographic metadata':
          #<mods:publisher>Printed by A. Imlay, 22, Long Acre</mods:publisher>
          originInfo = mdwrap_item.getElementsByTagName('mods:originInfo')[0]
          
          for originInfo_item in originInfo.childNodes:
            if originInfo_item.nodeName == 'mods:publisher':
              chapbooks_metadata[chapbook_id]['imprint'] = originInfo_item.firstChild.nodeValue
            if originInfo_item.nodeName == 'mods:dateIssued' and len(originInfo_item.attributes) == 0:
                chapbooks_metadata[chapbook_id]['date'] = originInfo_item.firstChild.nodeValue
  return chapbooks_metadata

def get_image_id_to_nls_page_id_map(chapbook_id_list, NLS_IMAGE_DIR):
  '''
  The images in the chapbook with id "104186119" have the following filenames:
  104186119/image/108614013.3.jpg
  104186119/image/108614049.3.jpg
  104186119/image/108614109.3.jpg
  ...
  However, these individual pages are hosted under a different URL:
  https://digital.nls.uk/108614047
  https://digital.nls.uk/108614107
  https://digital.nls.uk/108614251
  
  These URL can only be constructed by looking into the "structMap" section of 
  METS file corresponding to each chapbook.
  '''
  print('Computing image_id to NLS digital page_id map ...')
  image_id_to_nls_page_id_map = {}

  for chapbook_id in chapbook_id_list:
    if chapbook_id not in image_id_to_nls_page_id_map:
      image_id_to_nls_page_id_map[chapbook_id] = {}
    mets_fn = os.path.join(NLS_IMAGE_DIR, chapbook_id, chapbook_id + '-mets.xml')
    if not os.path.exists(mets_fn):
      print('missing: ' + mets_fn)
      continue
    mets = minidom.parse(mets_fn)
    structMap = mets.getElementsByTagName('structMap')[0].firstChild
    div_ID = structMap.attributes['ID'].value
    div_chapbook_id = div_ID.split('_')[1]
    if div_chapbook_id == chapbook_id:
      for child1 in structMap.childNodes:
        child1_ID = child1.attributes['ID'].value
        nls_digital_page_id = child1_ID.split('_')[1]
        for child2 in child1.childNodes:
          image_file_id = child2.attributes['FILEID'].value.split('_')[1]
          if image_file_id in image_id_to_nls_page_id_map[chapbook_id]:
            print('ERROR: collison in image_id_to_nls_page_id_map[%s]' % (chapbook_id))
          else:
            image_id_to_nls_page_id_map[chapbook_id][image_file_id] = nls_digital_page_id

    for findex in chapbook_id_list[chapbook_id]:
      fn = lisa['files'][findex]['src']
      fn_with_ext = fn.split('/')[2]
      fn_without_ext = os.path.splitext(fn_with_ext)[0]
      if fn_without_ext not in image_id_to_nls_page_id_map[chapbook_id]:
        print('ERROR: %s not found in image_id_to_nls_page_id_map[%s]' % (chapbook_id, fn_without_ext))

  return image_id_to_nls_page_id_map

def lisa_project_to_vise_folder(lisa_project_fn,
                                chapbooks_metadata,
                                image_id_to_nls_page_id_map,
                                VISE_PROJECT_DIR,
                                NLS_IMAGE_DIR):
  print('Generating contents of VISE project: %s' % (VISE_PROJECT_DIR))

  # check if the supplied folders are reasonablly correct
  if os.path.exists( os.path.join(VISE_PROJECT_DIR, 'data') ) or \
     os.path.exists( os.path.join(VISE_PROJECT_DIR, 'image_src') ) :
    print("Error: VISE project dir %s must be empty" % (VISE_PROJECT_DIR))
    return
  if not os.path.exists( os.path.join(NLS_IMAGE_DIR, '104184105') ):
    print("Error: NLS Chapbooks dir %s does not contain all the folders (e.g. 104184105)" % (NLS_IMAGE_DIR))
    return
  
  with open(lisa_project_fn) as f:
    lisa = json.load(f)

  # collect a list of all file_indices that contain a region
  sel_findex_list = []
  for findex, file in enumerate(lisa['files']):
    if len(lisa['files'][findex]['regions']):
      sel_findex_list.append(findex)

  N = len(sel_findex_list)
  print('%d images contains illustration, writing images to %s' % (N, VISE_PROJECT_DIR))
  print('please wait, this takes some time ...')

  # copy images to $VISE/image_src folder
  vise_image_src_dir = os.path.join(VISE_PROJECT_DIR, 'image_src')
  vise_image_region_dir = os.path.join(VISE_PROJECT_DIR, 'image_regions')
  vise_data_dir = os.path.join(VISE_PROJECT_DIR, 'data')
  if not os.path.exists(vise_image_src_dir):
    os.mkdir(vise_image_src_dir)
  if not os.path.exists(vise_image_region_dir):
    os.mkdir(vise_image_region_dir)
  if not os.path.exists(vise_data_dir):
    os.mkdir(vise_data_dir)

  metadata_db_fn = os.path.join(vise_data_dir, 'metadata_db.sqlite')
  metadata_db = sqlite3.connect(metadata_db_fn)
  metadata_db.row_factory = sqlite3.Row
  metadata_db_cursor = metadata_db.cursor()

  ## copy images and generate filelist (used by VISE during image indexing)
  filelist_fn = os.path.join(vise_data_dir, 'filelist.txt')
  filelist_f = open(filelist_fn, 'w')
  file_id = 0
  is_first = True
  file_insert_values = []
  region_insert_values = []
  total_region_count = 0
  total_im_with_illus_count = 0

  for findex in sel_findex_list:
    fn = lisa['files'][findex]['src']
    if is_first:
      filelist_f.write(fn)
      is_first = False
    else:
      filelist_f.write('\n' + fn)

    ## copy images
    src_img_path = os.path.join(NLS_IMAGE_DIR, fn)
    vise_img_path = os.path.join(VISE_PROJECT_DIR, 'image_src', fn)
    vise_img_dir = os.path.dirname(vise_img_path)
    vise_img_region_path = os.path.join(VISE_PROJECT_DIR, 'image_regions', fn)
    vise_img_region_dir = os.path.dirname(vise_img_region_path)
    if not os.path.exists(vise_img_dir):
      os.makedirs(vise_img_dir)
    if not os.path.exists(vise_img_region_dir):
      os.makedirs(vise_img_region_dir)
    if not os.path.exists(vise_img_path):
      os.system('cp ' + src_img_path + ' ' + vise_img_path)
      #print('Copied ' + vise_img_path)

    if len(lisa['files'][findex]['regions']):
      total_im_with_illus_count = total_im_with_illus_count + 1

    ## file metadata
    # https://digital.nls.uk/chapbooks-printed-in-scotland/archive/104184106
    chapbook_id = fn.split('/')[0]
    fn_with_ext = fn.split('/')[2]
    fn_without_ext = os.path.splitext(fn_with_ext)[0]
    nls_digital_chapbook_page_id = ''
    if fn_without_ext not in image_id_to_nls_page_id_map[chapbook_id]:
      print('ERROR: %s not found in image_id_to_nls_page_id_map[%s]' % (chapbook_id, fn_without_ext))
    else:
      nls_digital_chapbook_page_id = image_id_to_nls_page_id_map[chapbook_id][fn_without_ext]
    file_insert_values.append( (file_id,
                                fn,
                                chapbook_id,
                                chapbooks_metadata[chapbook_id]['place_printed'],
                                chapbooks_metadata[chapbook_id]['place_printed_nation'],
                                chapbooks_metadata[chapbook_id]['place_published'],
                                chapbooks_metadata[chapbook_id]['place_sold'],
                                chapbooks_metadata[chapbook_id]['publisher'],
                                chapbooks_metadata[chapbook_id]['printer'],
                                chapbooks_metadata[chapbook_id]['bookseller'],
                                chapbooks_metadata[chapbook_id]['date'],
                                chapbooks_metadata[chapbook_id]['volume'],
                                chapbooks_metadata[chapbook_id]['item'],
                                chapbooks_metadata[chapbook_id]['imprint'],
                                nls_digital_chapbook_page_id,
                                '','','','','',''
    ) )

    ## region metadata
    region_index = 0
    img = Image.open(src_img_path)
    width, height = img.size
    img_filename_only = os.path.splitext(os.path.basename(fn))[0]
    for region in lisa['files'][findex]['regions']:
      ## rectangular regions are stored as [x, y, width, height]
      region_insert_values.append( (None, file_id, region_index, 'rect', ",".join( map(str, region) ), '', '', ''))
      ## extract region and save
      left = max(0, region[0])
      top  = max(0, region[1])
      right = min(left + region[2], width)
      bottom = min(top + region[3], height)
      if left == right or top == bottom:
        print('******* Warning: MALFORMED REGION [%d] %s: (%d,%d) : (%d,%d,%d,%d)' % (findex, fn, width, height, left, top, right, bottom))
      else:
        #print('%s: (%d,%d) : (%d,%d,%d,%d)' % (fn, width, height, left, top, right, bottom))
        img_region = img.crop((left, top, right, bottom))
        img_region_fn = os.path.join(vise_img_region_dir, img_filename_only + '-region-' + str(region_index) + '.jpg')
        img_region.save(img_region_fn)
      region_index = region_index + 1
      total_region_count = total_region_count + 1
    file_id = file_id + 1

  filelist_f.close()
  print('Total image with region count = %d' %(total_im_with_illus_count))
  print('Total region count = %d' %(total_region_count))

  metadata_db_cursor.execute('BEGIN TRANSACTION');
  sql = 'CREATE TABLE file_metadata( file_id INTEGER PRIMARY KEY, filename TEXT NOT NULL, chapbook_id TEXT NOT NULL, place_printed TEXT, place_printed_nation TEXT, place_published TEXT, place_sold TEXT, publisher TEXT, printer TEXT, bookseller TEXT, date TEXT, volume TEXT, item TEXT, imprint TEXT, nls_digital_chapbook_page_id, assigned_date TEXT, assigned_printer TEXT, assigned_publisher TEXT, assigned_place_printed TEXT, assigned_place_published TEXT, assigned_place_sold TEXT);'
  metadata_db_cursor.execute(sql);
  sql = 'CREATE TABLE region_metadata( region_id INTEGER PRIMARY KEY, file_id INTEGER NOT NULL, region_index TEXT NOT NULL, region_shape TEXT NOT NULL, region_points TEXT NOT NULL, illustration_group TEXT, illustration_version TEXT, keywords TEXT );'
  metadata_db_cursor.execute(sql);
  metadata_db_cursor.executemany('INSERT INTO file_metadata VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', file_insert_values);
  metadata_db_cursor.executemany('INSERT INTO region_metadata VALUES (?,?,?,?,?,?,?,?)', region_insert_values);
  metadata_db_cursor.execute('END TRANSACTION');

  metadata_db.commit()
  metadata_db.close()

if __name__=="__main__":
  parser = argparse.ArgumentParser(description="Convert manual annotations from a LISA project to COCO format")
  parser.add_argument("--lisa_project_fn",
                      required=True,
                      type=str,
                      help="location of LISA project containing annotations (e.g. /home/tlm/nls/nls-chapbooks-illustrations/data/annotations/step5-manual-verifcation-image-20000-to-47229.json")
  parser.add_argument("--vise_project_dir",
                      required=True,
                      type=str,
                      help="all the images and metadata will get written to this VISE folder (e.g. /home/tlm/nls/vise-project/")
  parser.add_argument("--nls_dataset_dir",
                      required=True,
                      type=str,
                      help="location of NLS Chapbooks dataset (e.g. /dataset/nls-data-chapbooks/)")

  parser.add_argument("--nls_metadata_json_dir",
                      required=True,
                      type=str,
                      help="extra metadata provided by the NLS in json format")

  args = parser.parse_args()

  with open(args.lisa_project_fn) as f:
    lisa = json.load(f)

  # collect a list of all file_indices that contain a region
  sel_findex_list = []
  for findex, file in enumerate(lisa['files']):
    if len(lisa['files'][findex]['regions']):
      sel_findex_list.append(findex)

  # create a list of all chapbook-id and their associated image files
  chapbook_id_list = {}
  for findex in sel_findex_list:
    fn = lisa['files'][findex]['src']
    chapbook_id = fn.split('/')[0]
    if chapbook_id not in chapbook_id_list:
      chapbook_id_list[chapbook_id] = []
    chapbook_id_list[chapbook_id].append(findex)

  image_id_to_nls_page_id_map = get_image_id_to_nls_page_id_map(chapbook_id_list,
                                                                args.nls_dataset_dir)
  chapbooks_metadata1 = {}
  chapbooks_metadata2 = load_chapbooks_metadata_from_json(chapbook_id_list,
                                                          chapbooks_metadata1,
                                                          args.nls_metadata_json_dir)
  chapbooks_metadata = load_chapbooks_metadata_from_mets(chapbook_id_list,
                                                         chapbooks_metadata2,
                                                         args.nls_dataset_dir)
  lisa_project_to_vise_folder(args.lisa_project_fn,
                              chapbooks_metadata,
                              image_id_to_nls_page_id_map,
                              args.vise_project_dir,
                              args.nls_dataset_dir)
