## Detect illustrations in a batch of images and export these detections in a JSON file
##
## Author:  Abhishek Dutta <adutta@robots.ox.ac.uk>
## Date:    2022-Jun-22

import argparse
import json

import os
import time
from typing import Text, Tuple, List

from absl import app
from absl import flags
from absl import logging

import numpy as np
from PIL import Image
import tensorflow.compat.v1 as tf

import hparams_config
import inference
import utils
from tensorflow.python.client import timeline  # pylint: disable=g-direct-tensorflow-import

def main():
    ## initialize and define the command line parser
    parser = argparse.ArgumentParser(prog='detect-illustration',
                                     description='Detect illustrations in early printed books (https://gitlab.com/vgg/nls-chapbooks-illustrations)')
    parser.add_argument("--model-name",
                        required=True,
                        type=str,
                        help="e.g. efficientdet-d0 or efficientdet-d2")
    parser.add_argument("--saved-model-dir",
                        required=True,
                        type=str,
                        help="path for saved model")
    parser.add_argument("--hparams",
                        required=True,
                        type=str,
                        help="efficientdet model hyper-parameters filename")
    parser.add_argument("--detection-threshold",
                        required=False,
                        type=float,
                        default=0.5,
                        help="all detections below this threshold are discarded")
    parser.add_argument("--input-image-dir",
                        required=True,
                        type=str,
                        help="either a pattern (e.g. /data/images/*.jpg) or a filename (e.g. /data/images/1.jpg)")
    parser.add_argument("--output-image-dir",
                        required=True,
                        type=str,
                        help="images containing detected bounding boxes are saved into this folder")
    parser.add_argument("--output-json-fn",
                        required=True,
                        type=str,
                        help="annotations are saved in this JSON file")
    
    args = parser.parse_args()

    batch_size = 1 # TODO: update code to process a batch of more than 1 image
    all_files = list(tf.io.gfile.glob(args.input_image_dir))
    print('applying detector to these files: ', all_files)
    num_batches = (len(all_files) + batch_size - 1) // batch_size

    model_config = hparams_config.get_detection_config(args.model_name)
    model_config.override(args.hparams)
    model_config.is_training_bn = False
    model_config.image_size = utils.parse_image_size(model_config.image_size)
    
    use_xla = False
    ckpt_path = None
    driver = inference.ServingDriver(
        args.model_name,
        ckpt_path,
        batch_size=batch_size,
        use_xla=use_xla,
        model_params=model_config.as_dict())
    driver.load(args.saved_model_dir)

    output_json = []
    print('\nShowing Detections\n')
    for i in range(num_batches):
        batch_files = all_files[i * batch_size:(i + 1) * batch_size]
        
        height, width = model_config.image_size
        images = [Image.open(f) for f in batch_files]
        if len(set([m.size for m in images])) > 1:
            # Resize only if images in the same batch have different sizes.
            images = [m.resize((height, width)) for m in images]
        raw_images = [np.array(m) for m in images]

        size_before_pad = len(raw_images)
        if size_before_pad < batch_size:
            padding_size = batch_size - size_before_pad
            raw_images += [np.zeros_like(raw_images[0])] * padding_size

        detections_bs = driver.serve_images(raw_images)
        detections_i = []
        
        for j in range(size_before_pad):
            img = driver.visualize(raw_images[j],
                                   detections_bs[j],
                                   line_thickness=4
            )
            img_id = str(i * batch_size + j)
            img_fn = batch_files[j]
            out_fn = os.path.join(args.output_image_dir, os.path.basename(img_fn))
            Image.fromarray(img).save(out_fn)
            
            print('[%s] %s' % (img_id, img_fn))
            for k in range(0, len(detections_bs[j])):
                confidence = detections_bs[j][k][5]
                if confidence < args.detection_threshold:
                    continue
                y0 = int(detections_bs[j][k][1])
                x0 = int(detections_bs[j][k][2])
                y1 = int(detections_bs[j][k][3])
                x1 = int(detections_bs[j][k][4])
                print('  [%d] (x0,y0)=(%d,%d), (x1,y1)=(%d,%d), confidence=%.2f' % (k, x0, y0, x1, y1, confidence))
                detections_i.append({
                    'x0':float(x0),
                    'y0':float(y0),
                    'x1':float(x1),
                    'y1':float(y1),
                    'confidence':float(confidence)
                })
            output_json.append( {
                'file_id':img_id,
                'filename':img_fn,
                'early_printed_illustrations': detections_i
                })
    print('\n')
    with open(args.output_json_fn, 'w') as f:
        json.dump(output_json, f)
        print('Detections written to %s' % (args.output_json_fn))
    print('Images containing detection bounding box saved to %s' % (args.output_image_dir))

if __name__ == '__main__':
    main()

